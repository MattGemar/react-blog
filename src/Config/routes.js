const routes = {
	HOME: '/',
	CONTACT: '/contact',
	ARTICLES: '/articles',
	EMAIL: 'email',
	TELEPHONE: 'telephone',
	ADMINARTICLE: '/admin/article',
	AUTHENTIFICATION: '/connexion',
};

export default routes;
