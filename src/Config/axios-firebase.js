import axios from 'axios';

const instance = axios.create({
	baseURL:
		'https://blog-react-5f8b7-default-rtdb.europe-west1.firebasedatabase.app/',
});

export default instance;
