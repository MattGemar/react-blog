// Import the functions you need from the SDKs you need
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
	apiKey: 'AIzaSyBx7z7OGZwImSOCJJF_1XXDcHfW5Psku4k',
	authDomain: 'blog-react-5f8b7.firebaseapp.com',
	databaseURL:
		'https://blog-react-5f8b7-default-rtdb.europe-west1.firebasedatabase.app',
	projectId: 'blog-react-5f8b7',
	storageBucket: 'blog-react-5f8b7.appspot.com',
	messagingSenderId: '98545113888',
	appId: '1:98545113888:web:3deb8eb4d828452e7d1545',
};

// Initialize Firebase
const fire = firebase.initializeApp(firebaseConfig);

export default fire;
