// Librairies
import React, { useEffect } from 'react';
import classes from './Contact.module.css';
import { Outlet, useNavigate } from 'react-router-dom';
import routes from '../../Config/routes';

function Contact(props) {
	const navigate = useNavigate();

	useEffect(() => {
		// Remplace componentDidUpdate
		document.title = 'Contact';
	});

	const emailClickedHandler = () => {
		navigate(routes.EMAIL);
	};

	const callClickedHandler = () => {
		navigate(routes.TELEPHONE);
	};

	return (
		<>
			<h1> Contact </h1>
			<p> Par quel moyen de contact souhaitez-vous échanger?</p>
			<button onClick={emailClickedHandler} className={classes.button}>
				Email
			</button>
			<button onClick={callClickedHandler} className={classes.button}>
				Téléphone
			</button>
			<Outlet />
		</>
	);
}

export default Contact;
