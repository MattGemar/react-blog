// Librairies
import React from 'react';
import classes from './AffichageArticles.module.css';

// Composants
import AffichageArticle from './AffichageArticle/AffichageArticle';

function AffichageArticles(props) {
	const articles = props.articles.map((article) => (
		<AffichageArticle article={article} key={article.id} />
	));

	return (
		<>
			<section className={[classes.AffichageArticles, 'container'].join(' ')}>
				{articles}
			</section>
		</>
	);
}

export default AffichageArticles;
