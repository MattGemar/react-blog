// Librairies
import React from 'react';
import classes from './AffichageArticle.module.css';
import routes from '../../../Config/routes';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

function AffichageArticle(props) {
	return (
		<>
			<Link
				className={classes.link}
				to={routes.ARTICLES + '/' + props.article.slug}>
				<div className={classes.AffichageArticle}>
					<h2>{props.article.titre}</h2>
					<p>{props.article.accroche}</p>
					<small>{props.article.auteur}</small>
				</div>
			</Link>
		</>
	);
}

AffichageArticle.propTypes = {
	article: PropTypes.object,
};

export default AffichageArticle;
