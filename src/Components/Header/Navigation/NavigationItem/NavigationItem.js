// Librairies
import React from 'react';
//import NavLink from '../../../../UI/NavLink/Navlink'
import classes from './NavigationItem.module.css';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

function NavigationItem(props) {
	return (
		<li className={classes.NavigationItem}>
			<NavLink
				end={props.end}
				to={props.to}
				className={({ isActive }) => (isActive ? classes.active : null)}>
				{props.children}
			</NavLink>
		</li>
	);
}

NavigationItem.propTypes = {
	to: PropTypes.string,
	children: PropTypes.string,
};

export default NavigationItem;
