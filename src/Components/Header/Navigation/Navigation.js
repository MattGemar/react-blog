// Librairies
import React from 'react';
import classes from './Navigation.module.css';
import fire from '../../../Config/firebase';
import { useNavigate } from 'react-router';
import routes from '../../../Config/routes';
// Composants
import NavigationItem from './NavigationItem/NavigationItem';
import { toast } from 'react-toastify';

function Navigation(props) {
	// State
	const navigate = useNavigate();
	// Méthodes
	const logoutClickedHandler = () => {
		fire.auth().signOut();
		toast.success('Vous êtes déconnecté');
		navigate(routes.HOME, { replace: true });
	};
	return (
		<ul className={classes.Navigation}>
			<NavigationItem end to={routes.HOME}>
				{' '}
				Accueil{' '}
			</NavigationItem>
			<NavigationItem to={routes.CONTACT}> Contact </NavigationItem>
			<NavigationItem to={routes.ARTICLES}> Articles </NavigationItem>
			{props.user ? (
				<NavigationItem to={routes.ADMINARTICLE}>
					{' '}
					Ajouter Article{' '}
				</NavigationItem>
			) : null}
			{props.user ? null : (
				<NavigationItem end to={routes.AUTHENTIFICATION}>
					{' '}
					Connexion{' '}
				</NavigationItem>
			)}
			{props.user ? (
				<button className={classes.logout} onClick={logoutClickedHandler}>
					{' '}
					Déconnexion{' '}
				</button>
			) : null}
		</ul>
	);
}

export default Navigation;
