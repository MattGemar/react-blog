// Librairies
import React from 'react';
import classes from './Footer.module.css';

function Footer(props) {
	return (
		<footer className={classes.Footer}>
			<div className='container'>2021 © MattGemar's Blog</div>
		</footer>
	);
}

export default Footer;
