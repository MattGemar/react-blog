// Librairies
import React, { useState, useEffect } from 'react';
import './App.css';
import { Routes, Route } from 'react-router-dom';
import fire from './Config/firebase';

// Composants
import Layout from './hoc/Layout/Layout';
import Home from './Containers/Home/Home';
import Contact from './Components/Contact/Contact';
import Articles from './Containers/Articles/Articles';
import Article from './Containers/Articles/Article/Article';
import NotFound from './Containers/NotFound/NotFound';
import Email from './Components/Contact/Email/Email';
import Telephone from './Components/Contact/Telephone/Telephone';
import ManageArticle from './Containers/Admin/Articles/ManageArticle/ManageArticle';
import Authentification from './Containers/Security/Authentification/Authentification';
import routes from './Config/routes';

function App() {
	// State
	const [user, setUser] = useState('');
	// Cycle de vie
	useEffect(() => {
		// Remplace componentDidMount
		console.log('[App.js] UseEffect (mount)');
		authListener();
		return () => {
			// Remplace componentWillUnmount
			console.log('[App.js] UseEffect (unmount)');
		};
	}, []);

	// Méthodes
	const authListener = () => {
		fire.auth().onAuthStateChanged((user) => {
			if (user) {
				setUser(user);
			} else {
				setUser('');
			}
		});
	};
	return (
		<div className='App'>
			<Layout user={user}>
				<Routes>
					<Route exact path={routes.HOME} element={<Home />} />
					<Route path={routes.CONTACT} element={<Contact />}>
						<Route path={routes.EMAIL} element={<Email />} />
						<Route path={routes.TELEPHONE} element={<Telephone />} />
					</Route>
					<Route exact path={routes.ARTICLES} element={<Articles />} />
					<Route
						exact
						path={routes.ARTICLES + '/:slug'}
						element={<Article user={user} />}
					/>
					{user ? (
						<Route
							exact
							path={routes.ADMINARTICLE}
							element={<ManageArticle />}
						/>
					) : null}
					{user ? null : (
						<Route
							exact
							path={routes.AUTHENTIFICATION}
							element={<Authentification />}
						/>
					)}
					<Route path='*' element={<NotFound />} />
				</Routes>
			</Layout>
		</div>
	);
}

export default App;
