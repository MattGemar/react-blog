// Librairies
import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import checkValidity from '../../../Shared/Services/checkValidityService/checkValidity';
import classes from './Authentification.module.css';
import routes from '../../../Config/routes';
import fire from '../../../Config/firebase';

// Composants
import Input from '../../../UI/Input/Input';
import { toast } from 'react-toastify';

function Authentification() {
	const [inputs, setInputs] = useState({
		email: {
			elementType: 'input',
			elementConfig: {
				type: 'email',
				placeholder: 'Email',
			},
			value: '',
			label: 'Adresse email',
			valid: false,
			validation: {
				required: true,
				email: true,
				message: "L'adresse email n'est pas valide.",
			},
			touched: false,
		},
		password: {
			elementType: 'input',
			elementConfig: {
				type: 'password',
				placeholder: 'Mot de passe',
			},
			value: '',
			label: 'Mot de passe',
			valid: false,
			validation: {
				required: true,
				minLength: 6,
				message:
					'Le mot de passe doit être renseigné et comporter plus de 6 caractères.',
			},
			touched: false,
		},
	});

	const [formValidation, setFormValidation] = useState(false);
	const [emailError, setEmailError] = useState(false);
	const [loginError, setLoginError] = useState(false);
	const navigate = useNavigate();

	useEffect(() => {
		// Remplace componentDidUpdate
		document.title = 'Authentification';
	});

	// Methodes
	const checkFormValidity = (form) => {
		let formIsValid = true;
		for (const input in form) {
			formIsValid = form[input].valid && formIsValid;
		}
		return formIsValid;
	};

	const loginClickHandler = () => {
		const user = {
			email: inputs.email.value,
			password: inputs.password.value,
		};
		fire
			.auth()
			.signInWithEmailAndPassword(user.email, user.password)
			.then((response) => {
				toast.success('Vous êtes connecté');
				navigate(routes.HOME);
			})
			.catch((error) => {
				switch (error.code) {
					case 'auth/invalid-email':
						setLoginError(true);
						break;
					case 'auth/user-disabled':
						setLoginError(true);
						break;
					case 'auth/user-not-found':
						setLoginError(true);
						break;
					default:
						break;
				}
			});
	};

	const registerClickHandler = () => {
		const user = {
			email: inputs.email.value,
			password: inputs.password.value,
		};
		fire
			.auth()
			.createUserWithEmailAndPassword(user.email, user.password)
			.then((response) => {
				toast.success('Vous êtes inscrit');
				navigate(routes.HOME);
			})
			.catch((error) => {
				// Adresse email déjà utilisée
				switch (error.code) {
					case 'auth/email-already-in-use':
						setEmailError(true);
						break;
					default:
						break;
				}
				console.log(error);
			});
	};

	const inputChangedHandler = (e, id) => {
		// Change la valeur
		const nouveauxInputs = { ...inputs };
		nouveauxInputs[id].value = e.target.value;
		nouveauxInputs[id].touched = true;

		// Vérification de la valeur
		nouveauxInputs[id].valid = checkValidity(
			nouveauxInputs[id].value,
			nouveauxInputs[id].validation
		);
		setInputs(nouveauxInputs);
		// Vérification du formulaire
		setFormValidation(checkFormValidity(nouveauxInputs));
	};

	const formHandler = (e) => {
		e.preventDefault();
	};

	// Variables
	const formElementsArray = [];
	for (const key in inputs) {
		formElementsArray.push({
			id: key,
			config: inputs[key],
		});
	}

	const form = (
		<form onSubmit={(e) => formHandler(e)}>
			{formElementsArray.map((formElement) => (
				<Input
					key={formElement.id}
					id={formElement.id}
					value={formElement.config.value}
					label={formElement.config.label}
					type={formElement.config.elementType}
					config={formElement.config.elementConfig}
					valid={formElement.config.valid}
					touched={formElement.config.touched}
					errorMessage={formElement.config.validation.message}
					handleChange={(e) => inputChangedHandler(e, formElement.id)}
				/>
			))}
			<div className={classes.buttons}>
				<button
					onClick={loginClickHandler}
					disabled={!formValidation}
					className={classes.button}>
					{' '}
					Connexion{' '}
				</button>
				<button
					onClick={registerClickHandler}
					disabled={!formValidation}
					className={classes.button}>
					{' '}
					Inscription{' '}
				</button>
			</div>
		</form>
	);

	return (
		<>
			<h1>Authentification</h1>
			<div className={classes.form}>
				{loginError ? (
					<div className={classes.alert}>
						{' '}
						Impossible de vous authentifier.{' '}
					</div>
				) : null}
				{emailError ? (
					<div className={classes.alert}>
						{' '}
						Cette adresse Email est déjà utilisée.{' '}
					</div>
				) : null}
				{form}
			</div>
		</>
	);
}

export default Authentification;
