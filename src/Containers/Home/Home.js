// Librairies
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from '../../Config/axios-firebase';
import routes from '../../Config/routes';
import classes from './Home.module.css';

// Composants
import AffichageArticles from '../../Components/AffichageArticles/AffichageArticles';

function Home(props) {
	// State
	const [articles, setArticles] = useState([]);

	// Cycle de vie
	useEffect(() => {
		// Remplace componentDidMount
		console.log('[App.js] UseEffect (mount)');
		recupererArticles();
		return () => {
			// Remplace componentWillUnmount
			console.log('[App.js] UseEffect (unmount)');
		};
	}, []);

	useEffect(() => {
		// Remplace componentDidUpdate
		document.title = 'Accueil';
	});

	// Methodes
	const recupererArticles = () => {
		axios
			.get('/articles.json')
			.then((response) => {
				let listeArticles = [];
				for (const key in response.data) {
					listeArticles.push({
						...response.data[key],
						id: key,
					});
				}
				// Chronologie
				// Tri
				// Limiter à trois
				listeArticles = listeArticles
					.reverse()
					.filter((article) => article.brouillon === 'false')
					.slice(0, 3);
				setArticles(listeArticles);
			})
			.catch((error) => {
				console.log(error);
			});
	};
	return (
		<>
			<h1>Accueil</h1>
			<AffichageArticles articles={articles} />
			<div className='container'>
				<div className={classes.mainLink}>
					<Link to={routes.ARTICLES}>
						Voir les articles &nbsp;
						<svg
							xmlns='http://www.w3.org/2000/svg'
							width='16'
							height='16'
							fill='currentColor'
							viewBox='0 0 16 16'>
							<path
								fillRule='evenodd'
								d='M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z'
							/>
						</svg>
					</Link>
				</div>
			</div>
		</>
	);
}

export default Home;
