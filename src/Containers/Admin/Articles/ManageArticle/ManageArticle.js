// Librairies
import React, { useState, useEffect } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import classes from './ManageArticle.module.css';
import axios from '../../../../Config/axios-firebase';
import routes from '../../../../Config/routes';
import checkValidity from '../../../../Shared/Services/checkValidityService/checkValidity';
import fire from '../../../../Config/firebase';
import { toast } from 'react-toastify';

// Composants
import Input from '../../../../UI/Input/Input';

function ManageArticle(props) {
	// State
	const { state } = useLocation();

	const [inputs, setInputs] = useState({
		titre: {
			elementType: 'input',
			elementConfig: {
				type: 'text',
				placeholder: "Titre de l'article",
			},
			value: state && state.article ? state.article.titre : '',
			label: 'Titre',
			valid: state && state.article,
			validation: {
				required: true,
				minLength: 5,
				maxLength: 85,
				message:
					'Le titre doit avoir une longueur minimum de 5 lettres et maximum de 85 lettres.',
			},
			touched: false,
		},
		accroche: {
			elementType: 'textArea',
			elementConfig: {},
			value: state && state.article ? state.article.accroche : '',
			label: "Accroche de l'article",
			valid: state && state.article,
			validation: {
				required: true,
				minLength: 10,
				maxLength: 140,
				message: "L'accroche doit avoir un taille entre 10 et 140 caractères..",
			},
			touched: false,
		},
		contenu: {
			elementType: 'textArea',
			elementConfig: {},
			value: state && state.article ? state.article.contenu : '',
			label: "Contenu de l'article",
			valid: state && state.article,
			validation: {
				required: true,
				message: 'Le contenu ne doit pas être vide.',
			},
			touched: false,
		},
		auteur: {
			elementType: 'input',
			elementConfig: {
				type: 'text',
				placeholder: "Auteur de l'article",
			},
			value: state && state.article ? state.article.auteur : '',
			label: 'Auteur',
			valid: state && state.article,
			validation: {
				required: true,
				message: "Il faut un auteur pour l'article.",
			},
			touched: false,
		},
		brouillon: {
			elementType: 'select',
			elementConfig: {
				options: [
					{ value: true, displayValue: 'Brouillon' },
					{ value: false, displayValue: 'Publié' },
				],
			},
			value: state && state.article ? state.article.brouillon : true,
			label: 'Etat',
			valid: true,
			validation: {},
		},
	});
	const [formValidation, setFormValidation] = useState(state && state.article);
	const navigate = useNavigate();

	// Cycle de vie
	useEffect(() => {
		// Remplace componentDidMount
		console.log('[App.js] UseEffect (mount)');
		return () => {
			// Remplace componentWillUnmount
			console.log('[App.js] UseEffect (unmount)');
		};
	}, []);

	useEffect(() => {
		// Remplace componentDidUpdate
		document.title = 'Gestion des articles';
	});

	const checkFormValidity = (form) => {
		let formIsValid = true;
		for (const input in form) {
			formIsValid = form[input].valid && formIsValid;
		}
		return formIsValid;
	};

	const inputChangedHandler = (e, id) => {
		// Change la valeur
		const nouveauxInputs = { ...inputs };
		nouveauxInputs[id].value = e.target.value;
		nouveauxInputs[id].touched = true;

		// Vérification de la valeur
		nouveauxInputs[id].valid = checkValidity(
			nouveauxInputs[id].value,
			nouveauxInputs[id].validation
		);
		setInputs(nouveauxInputs);
		// Vérification du formulaire
		setFormValidation(checkFormValidity(nouveauxInputs));
	};

	const slugify = (str) => {
		str = str.replace(/^\s+|\s+$/g, ''); // trim
		str = str.toLowerCase();

		// remove accents, swap ñ for n, etc
		const from = 'àáãäâèéëêìíïîòóöôùúüûñç·/_,:;';
		const to = 'aaaaaeeeeiiiioooouuuunc------';

		for (let i = 0, l = from.length; i < l; i++) {
			str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
		}

		str = str
			.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
			.replace(/\s+/g, '-') // collapse whitespace and replace by -
			.replace(/-+/g, '-'); // collapse dashes

		return str;
	};

	const formHandler = (e) => {
		e.preventDefault();
		if (formValidation) {
			const slug = slugify(inputs.titre.value);
			const nouvelArticle = {
				titre: inputs.titre.value,
				accroche: inputs.accroche.value,
				contenu: inputs.contenu.value,
				auteur: inputs.auteur.value,
				brouillon: inputs.brouillon.value,
				date: Date.now(),
				slug: slug,
			};
			fire
				.auth()
				.currentUser.getIdToken()
				.then((token) => {
					if (state && state.article) {
						axios
							.put(
								'/articles/' + state.article.id + '.json?auth=' + token,
								nouvelArticle
							)
							.then((response) => {
								console.log(response);
								toast.success('article modifié');
								navigate(routes.ARTICLES + '/' + nouvelArticle.slug, {
									replace: 'true',
								});
							})
							.catch((error) => {
								console.log(error);
							});
					} else {
						axios
							.post('/articles.json?auth=' + token, nouvelArticle)
							.then((response) => {
								console.log(response);
								toast.success('article ajouté');
								navigate(routes.ARTICLES, { replace: 'true' });
							})
							.catch((error) => {
								console.log(error);
							});
					}
				});
		}
	};

	// Variables
	const formElementsArray = [];
	for (const key in inputs) {
		formElementsArray.push({
			id: key,
			config: inputs[key],
		});
	}

	const form = (
		<form className={classes.ManageArticle} onSubmit={(e) => formHandler(e)}>
			{formElementsArray.map((formElement) => (
				<Input
					key={formElement.id}
					id={formElement.id}
					value={formElement.config.value}
					label={formElement.config.label}
					type={formElement.config.elementType}
					config={formElement.config.elementConfig}
					valid={formElement.config.valid}
					touched={formElement.config.touched}
					errorMessage={formElement.config.validation.message}
					handleChange={(e) => inputChangedHandler(e, formElement.id)}
				/>
			))}
			<div className={classes.submit}>
				<input type='submit' value='Envoyer' disabled={!formValidation} />
			</div>
		</form>
	);

	return (
		<div className='container'>
			<h1>{state && state.article ? 'Modifier article' : 'Ajouter article'}</h1>
			{form}
		</div>
	);
}

export default ManageArticle;
