// Librairies
import React, { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import axios from '../../../Config/axios-firebase';
import routes from '../../../Config/routes';
import classes from './Article.module.css';
import moment from 'moment';
import 'moment/locale/fr';

function Article(props) {
	const { slug } = useParams();
	const navigate = useNavigate();
	// State
	const [article, setArticle] = useState({});

	// Cycle de vie
	useEffect(() => {
		// Remplace componentDidMount
		console.log('[App.js] UseEffect (mount)');
		recupererArticle();
		return () => {
			// Remplace componentWillUnmount
			console.log('[App.js] UseEffect (unmount)');
		};
	});

	useEffect(() => {
		// Remplace componentDidUpdate
		document.title = article.titre;
	});

	// Methodes
	const recupererArticle = () => {
		axios
			.get('/articles.json?orderBy="slug"&equalTo="' + slug + '"')
			.then((response) => {
				if (Object.keys(response.data).length === 0) {
					toast.error("Cet article n'existe pas");
					navigate(routes.HOME, { replace: true });
				}
				for (const key in response.data) {
					setArticle({
						...response.data[key],
						id: key,
					});
				}
			})
			.catch((error) => {
				console.log(error);
			});
	};

	const afficherBoutons = () => {
		return (
			<div className={classes.button}>
				<button onClick={modifierClickHandler}> Modifier </button>
				<button onClick={supprimerClickHandler}> Supprimer </button>
			</div>
		);
	};

	const supprimerClickHandler = () => {
		props.user
			.getIdToken()
			.then((token) => {
				axios
					.delete('/articles/' + article.id + '.json?auth=' + token)
					.then((response) => {
						console.log(response);
						toast.success('article supprimé');
						navigate(routes.HOME, { replace: true });
					})
					.catch((error) => {
						console.log(error);
					});
			})
			.catch((error) => {
				console.log(error);
			});
	};

	const modifierClickHandler = () => {
		navigate(routes.ADMINARTICLE, { state: { article: article } });
	};

	// Variables

	// const date = new Date(article.date).toLocaleDateString()
	moment.locale('fr');
	const date = moment.unix(article.date / 1000).calendar();

	return (
		<div className='container'>
			<h1>{article.titre}</h1>
			<div className={classes.contenu}>
				<div className={classes.lead}>{article.accroche}</div>
				{article.contenu}
				{props.user ? afficherBoutons() : null}
			</div>
			<div className={classes.auteur}>
				<b>{article.auteur}</b>
				<span> Publié {date}</span>
				{article.brouillon === 'true' ? (
					<span className={classes.badge}> Brouillon </span>
				) : null}
			</div>
		</div>
	);
}

export default Article;
