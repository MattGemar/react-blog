// Librairies
import React, { useState, useEffect } from 'react';
import axios from '../../Config/axios-firebase';

// Composants
import AffichageArticles from '../../Components/AffichageArticles/AffichageArticles';

function Articles(props) {
	// State
	const [articles, setArticles] = useState([]);

	// Cycle de vie
	useEffect(() => {
		// Remplace componentDidMount
		console.log('[App.js] UseEffect (mount)');
		recupererArticles();
		return () => {
			// Remplace componentWillUnmount
			console.log('[App.js] UseEffect (unmount)');
		};
	}, []);
	useEffect(() => {
		// Remplace componentDidUpdate
		document.title = 'Articles';
	});

	// Methodes
	const recupererArticles = () => {
		axios
			.get('/articles.json')
			.then((response) => {
				let listeArticles = [];
				for (const key in response.data) {
					listeArticles.push({
						...response.data[key],
						id: key,
					});
				}
				listeArticles = listeArticles
					.reverse()
					.filter((article) => article.brouillon === 'false');
				setArticles(listeArticles);
			})
			.catch((error) => {
				console.log(error);
			});
	};

	return (
		<>
			<h1> Articles </h1>
			<AffichageArticles articles={articles} />
		</>
	);
}

export default Articles;
