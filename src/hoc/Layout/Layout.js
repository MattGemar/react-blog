// Librairies
import React from 'react';
import classes from './Layout.module.css';
import 'react-toastify/dist/ReactToastify.css';
// Composants
import Header from '../../Components/Header/Header';
import Footer from '../../Components/Footer/Footer';
import { ToastContainer } from 'react-toastify';

function Layout(props) {
	return (
		<div className={classes.layout}>
			{/* Header */}
			<Header className={classes.header} user={props.user} />
			<div className={classes.content}>{props.children}</div>
			<ToastContainer
				autoClose='10000'
				hideProgressBar='true'
				closeOnClick='false'
				pauseOnHover='false'
				position='bottom-right'
			/>
			{/* Footer */}
			<Footer />
		</div>
	);
}

/*
  - Header
    - logo
    - Navigation
      - NavigationItem
*/

export default Layout;
